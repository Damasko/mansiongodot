extends ImmediateGeometry

const height = 4.0;
const size = 1.0;

const v1 = Vector3(-size, 0.0,-size);
const v2 = Vector3(-size, 0.0, size);
const v3 = Vector3(-size, height, size); 
const v4 = Vector3(size, height,-size);
const v5 = Vector3(-size,0.0,-size);
const v6 = Vector3(-size, height,-size);
const v7 = Vector3(size,0.0, size);
const v8 = Vector3(-size,0.0,-size);
const v9 = Vector3(size,0.0,-size);
const v10 = Vector3(size, height,-size);
const v11 = Vector3(size,0.0,-size);
const v12 = Vector3(-size,0.0,-size);
const v13 = Vector3(-size,0.0,-size);
const v14 = Vector3(-size, height, size);
const v15 = Vector3(-size, height,-size);
const v16 = Vector3(size,0.0, size);
const v17 = Vector3(-size,0.0, size);
const v18 = Vector3(-size,0.0,-size);
const v19 = Vector3(-size,0.0, size);
const v20 = Vector3(-size, height, size); 
const v21 = Vector3(size,0.0, size);
const v22 = Vector3(size,0.0,-size);
const v23 = Vector3(size, height, size);
const v24 = Vector3(size, height,-size);
const v25 = Vector3(size, height, size);
const v26 = Vector3(size,0.0,-size);
const v27 = Vector3(size,0.0, size);
const v28 = Vector3(size, height,-size);
const v29 = Vector3(size, height, size);
const v30 = Vector3(-size, height,-size);
const v31 = Vector3(-size, height,-size); 
const v32 = Vector3(size, height, size); 
const v33 = Vector3(-size, height, size);
const v34 = Vector3(-size, height, size);
const v35 = Vector3(size, height, size);
const v36 = Vector3(size,0.0, size);


var n1 = Vector3(-size, 0.0, 0.0);
var n2 = Vector3(0.0, 0.0, -size);
var n3 = Vector3(0.0,-size, 0.0);
var n4 = Vector3(0.0, 0.0, -size);
var n5 = Vector3(-size, 0.0, 0.0);
var n6 = Vector3(0.0, -size, 0.0);
var n7 = Vector3(0.0, 0.0, size);
var n8 = Vector3(size, 0.0, 0.0);
var n9 = Vector3(size, 0.0, 0.0);
var n10 = Vector3(0.0, size, 0.0);
var n11 = Vector3(0.0, size, 0.0);
var n12 = Vector3(0.0, 0.0, size);

const uv1 = Vector2(0, 0);
const uv2 = Vector2(0, 1);
const uv3 = Vector2(1, 1);
const uv4 = Vector2(1, 0);

var origin = Vector3(0, 0, 0);
var final_pos = Vector3(0, 0, 0);
var floors = 1;

var enabled = false;
var has_origin = false;
var material = preload("res://Assets/Scenes/transparent_building.tres");
var blue_color = Color(0, 0, 0, 1);
var red_color = Color(1,0.5,0.376471,0.294118);

var gridMap = null;

var surf_tool = SurfaceTool.new();

var debug_enabled = false;
var can_build = true;

var items = [];

var start_x = 0;
var end_x = 0;
var start_z = 0;
var end_z = 0;

func _ready():
	
	set_process(false);
	enabled = false;
	material_override = material;
	blue_color = material_override.get("albedo_color");

func _set_enabled(state):
	
	clear();
	set_process(state);

	enabled = state;
	
func _set_grid_map(newGridMap):
	
	gridMap = newGridMap;
	gridMap.cell_size = Vector3(size, height, size);

func _process(delta):
	
	clear();

	if is_valid_space():
		material_override.set("albedo_color", blue_color);
		
	else:
		material_override.set("albedo_color", red_color);

	draw_cubes();
	
func draw_cubes():
	
	#var cubes = origin - final_pos;
	var start_x = origin.x;
	var end_x = final_pos.x;
	var start_z = origin.z;
	var end_z = final_pos.z;
	
	if origin.x > final_pos.x:
		start_x = final_pos.x;
		end_x = origin.x;
	
	if origin.z > final_pos.z:
		start_z = final_pos.z; 
		end_z = origin.z;
	
	for k in floors:
	
		for i in range(start_z, end_z + 1, 2):
			for j in range(start_x, end_x + 1, 2):
				draw_cube1(Vector3(j, origin.y + k * 4, i));
	
func draw_cube1(draw_pos):
	
	# Clean up before drawing.
	#clear();
	
	# Begin draw.
	begin(Mesh.PRIMITIVE_TRIANGLES);

	set_normal(n1);
	#set_uv(uv1);
	add_vertex(v1 + draw_pos);
	add_vertex(v2 + draw_pos);
	add_vertex(v3 + draw_pos);
	
	
	set_normal(n2);
	#set_uv(uv1);
	add_vertex(v4 + draw_pos);
	add_vertex(v5 + draw_pos);
	add_vertex(v6 + draw_pos);
	
	
	set_normal(n3);
	#set_uv(uv2);
	add_vertex(v7 + draw_pos);
	add_vertex(v8 + draw_pos);
	add_vertex(v9 + draw_pos);
	
	set_normal(n4);
	#set_uv(uv2);
	add_vertex(v10 + draw_pos);
	add_vertex(v11 + draw_pos);
	add_vertex(v12 + draw_pos);
	
	
	set_normal(n5);
	#set_uv(uv3);
	add_vertex(v13 + draw_pos);
	add_vertex(v14 + draw_pos);
	add_vertex(v15 + draw_pos);
	
	set_normal(n6);
	#set_uv(uv3);
	add_vertex(v16 + draw_pos);
	add_vertex(v17 + draw_pos);
	add_vertex(v18 + draw_pos);
	
	set_normal(n7);
	#set_uv(uv4);
	add_vertex(v19 + draw_pos);
	add_vertex(v20 + draw_pos);
	add_vertex(v21 + draw_pos);
	
	set_normal(n8);
	#set_uv(uv4);
	add_vertex(v22 + draw_pos);
	add_vertex(v23 + draw_pos);
	add_vertex(v24 + draw_pos);
	
	set_normal(n9);
	#set_uv(uv1);
	add_vertex(v25 + draw_pos);
	add_vertex(v26 + draw_pos);
	add_vertex(v27 + draw_pos);
	
	set_normal(n10);
	#set_uv(uv1);
	add_vertex(v28 + draw_pos);
	add_vertex(v29 + draw_pos);
	add_vertex(v30 + draw_pos);
	
	set_normal(n11);
	#set_uv(uv2);
	add_vertex(v31 + draw_pos);
	add_vertex(v32 + draw_pos);
	add_vertex(v33 + draw_pos);
	
	set_normal(n12);
	#set_uv(uv3);
	add_vertex(v34 + draw_pos);
	add_vertex(v35 + draw_pos);
	add_vertex(v36 + draw_pos);

	# End drawing.
	end();
	
func build_room():
	
	if !can_build:
		return;
	
	if gridMap == null:
		print("No GridMap configured");
		return

	print("ORIGEN: " + str(origin));
	print("FINAL: " + str(final_pos));
	
	#var cubes = origin - final_pos;
	var start_x = origin.x;
	var end_x = final_pos.x;
	var start_z = origin.z;
	var end_z = final_pos.z;
	
	if origin.x > final_pos.x:
		start_x = final_pos.x;
		end_x = origin.x;
	
	if origin.z > final_pos.z:
		start_z = final_pos.z; 
		end_z = origin.z;
	
	var rot = 0;
	var item = 0;
	
	var myQuaternion = Quat(Vector3(0, 1, 0.0), deg2rad(180));
	var myQuaternion_90 = Quat(Vector3(0, 1, 0.0), deg2rad(90));
	var myQuaternion_270 = Quat(Vector3(0, 1, 0.0), deg2rad(270));
	var cell_reverse = Basis(myQuaternion).get_orthogonal_index();
	var cell_90 = Basis(myQuaternion_90).get_orthogonal_index();
	var cell_270 = Basis(myQuaternion_270).get_orthogonal_index();
	
	for k in floors:
		for i in range(start_z, end_z + 1, 2):
			if i == start_z:
				item = 0;
				rot = cell_reverse;
			elif i == end_z:
				item = 0;
				rot = 0;
			else:
				item = 2;
				rot = 0;
				
			for j in range(start_x, end_x + 1, 2):
				
				if i != start_z and i != end_z:
					if j == start_x:
						item = 0;
						rot = cell_270;
					elif j == end_x:
						item = 0;
						rot = cell_90;	
					else:
						item = 2;
						rot = 0;
						
				if k > 0 and i != start_z and i != end_z and j != start_x and j != end_x:
						item = -1; # Aire
					
				gridMap.set_cell_item (j, k, i, item, rot);
		
		item = 6;
			
		gridMap.set_cell_item (start_x, k, start_z, item, cell_270);
		gridMap.set_cell_item (start_x, k, end_z, item, 0);
		gridMap.set_cell_item (end_x, k, start_z, item, cell_reverse);
		gridMap.set_cell_item (end_x, k, end_z, item, cell_90);
	
func build_room_debug():
	
	var meshinstance = MeshInstance.new()
	
	# v3 v4 v6 v10 v14 v15 v20 v23 v24 v25 v28 v29 v30 v31 v32 v33 v34 v35
	
	var height = Vector3(0, (floors - 1) * 4, 0);
	
	surf_tool.begin(Mesh.PRIMITIVE_TRIANGLES);
	
	surf_tool.add_normal(n1);
	#set_uv(uv1);
	surf_tool.add_vertex(v1 + origin);
	surf_tool.add_vertex(v2 + origin);
	surf_tool.add_vertex(v3 + origin + height);
	
	surf_tool.add_normal(n1);
	#set_uv(uv3);
	surf_tool.add_vertex(v13 + origin);
	surf_tool.add_vertex(v14 + origin + height);
	surf_tool.add_vertex(v15 + origin + height);

	
	### Cara lateral derecha
	surf_tool.add_normal(n3);
	#set_uv(uv1);
	surf_tool.add_vertex(v4 + origin + height);
	surf_tool.add_vertex(v5 + origin);
	surf_tool.add_vertex(v6 + origin + height);
	
	surf_tool.add_normal(n3);
	#set_uv(uv2);
	surf_tool.add_vertex(v10 + origin + height);
	surf_tool.add_vertex(v11 + origin);
	surf_tool.add_vertex(v12 + origin);

	### Cara inferior
	surf_tool.add_normal(n4);
	#set_uv(uv2);
	surf_tool.add_vertex(v7 + origin);
	surf_tool.add_vertex(v8 + origin);
	surf_tool.add_vertex(v9 + origin);
	
	surf_tool.add_normal(n4);
	#set_uv(uv3);
	surf_tool.add_vertex(v16 + origin);
	surf_tool.add_vertex(v17 + origin);
	surf_tool.add_vertex(v18 + origin);

	### Cara lateral izq
	surf_tool.add_normal(n7);
	#set_uv(uv4);
	surf_tool.add_vertex(v19 + origin);
	surf_tool.add_vertex(v20 + origin + height);
	surf_tool.add_vertex(v21 + origin);
	
	surf_tool.add_normal(n7);
	#set_uv(uv3);
	surf_tool.add_vertex(v34 + origin + height);
	surf_tool.add_vertex(v35 + origin + height);
	surf_tool.add_vertex(v36 + origin);
	
	### Cara posterior
	surf_tool.add_normal(n6);
	#set_uv(uv4);
	surf_tool.add_vertex(v22 + origin);
	surf_tool.add_vertex(v23 + origin + height);
	surf_tool.add_vertex(v24 + origin + height);
	
	surf_tool.add_normal(n6);
	#set_uv(uv1);
	surf_tool.add_vertex(v25 + origin + height);
	surf_tool.add_vertex(v26 + origin);
	surf_tool.add_vertex(v27 + origin);

	### Cara superior
	surf_tool.add_normal(n11);
	#set_uv(uv1);
	surf_tool.add_vertex(v28 + origin + height);
	surf_tool.add_vertex(v29 + origin + height);
	surf_tool.add_vertex(v30 + origin + height);
	
	surf_tool.add_normal(n11);
	#set_uv(uv2);
	surf_tool.add_vertex(v31 + origin + height);
	surf_tool.add_vertex(v32 + origin + height);
	surf_tool.add_vertex(v33 + origin + height);
	
	var new_material = SpatialMaterial.new();
	new_material.set("albedo_color", Color(1, 0, 0, 1));
	surf_tool.set_material(new_material);
	
	meshinstance.mesh = surf_tool.commit();
	
	get_parent().add_child(meshinstance);
	
	_unset_origin();
	
func is_valid_space():
	
	if gridMap != null:
		for k in floors:
			for i in range(start_z, end_z + 1, 2):
				for j in range(start_x, end_x + 1, 2):
					if gridMap.get_cell_item(j, k, i ) !=  GridMap.INVALID_CELL_ITEM:
						return false;
	
	return abs(origin.x - final_pos.x) > 3 and abs(origin.z - final_pos.z) > 3;
	
func _set_origin(position):
	
	origin = position;
	final_pos = position;
	has_origin = true;
	
func _unset_origin():
	
	origin = null;
	has_origin = false;
