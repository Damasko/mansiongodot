extends Spatial

var camera = null;
var room_builder = null;

func _ready():
	
	set_process(true);
	camera = $Camera;
	room_builder = $RoomBuilder;
	room_builder._set_grid_map($GridMap);

func _input(event):
	
	if event is InputEventMouseButton and event.pressed and event.button_index == 2:
		if room_builder.enabled:
			room_builder._set_enabled(false);
		else:
			set_physics_process(true);
			room_builder._set_enabled(true);
			_build_room(event.position);

"""
func _physics_process(delta):	

	var space_state = get_world().get_direct_space_state();
	var result = space_state.intersect_ray(from, to, [self]);
	print("result: ", result);    #<--------   always comes out empty
	set_physics_process(false);
"""
		
func _get_world_mouse_point(position):
	
	if camera.projection == Camera.PROJECTION_ORTHOGONAL:
		var from = camera.project_ray_origin(position)
		return from + camera.project_ray_normal(position) * 100;
		
	else:
		print("Camera perspective: TODO Raycast");
		
func _build_room(position):

	var final_pos = _get_world_mouse_point(position);
	final_pos.x = floor(final_pos.x);
	final_pos.y = floor(final_pos.y);
	final_pos.z = floor(final_pos.z);

	print(final_pos);
	room_builder.origin = final_pos;
