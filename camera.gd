extends Camera

enum MOVEMENT_TYPE {
	
	MOVE_ON_BORDERS,
	MOVE_ON_CLICK	
	
}

enum MODE {
	
	GRIDMAP,
	FREEMODE	
	
}

export var MAX_MOUSE_LIMIT = 0.2;
export var CAM_SPEED = 20;
export var RAY_LENGTH = 100;
export var MTYPE = MOVEMENT_TYPE.MOVE_ON_BORDERS;
export var MODE_HANDLER = MODE.GRIDMAP;
var room_builder = null;
var screenResolution = Vector2(0, 0);

var from = Vector3(0, 0, 0);
var to = Vector3(0, 0, 0);
var event_pos = Vector2(0, 0);

func _ready():
	
	set_process(true);
	set_process_input(true);
	#set_physics_process(true);
	screenResolution = get_viewport().size;
	room_builder = get_parent().get_node("RoomBuilder");
	
func _process(delta):
	
	if MTYPE == MOVEMENT_TYPE.MOVE_ON_BORDERS:
		_move_on_borders(delta);
		
	elif MTYPE == MOVEMENT_TYPE.MOVE_ON_CLICK:
		_move_on_click(delta);
	
func _move_on_borders(delta):
	
	var mouseMotion = get_viewport().get_mouse_position();
	var movement = Vector3(0, 0, 0);

	mouseMotion /= screenResolution;
	
	if mouseMotion.x < MAX_MOUSE_LIMIT:
		movement.x -= delta * CAM_SPEED;
		movement.z += delta * CAM_SPEED;
		
	elif mouseMotion.x > 1.0 - MAX_MOUSE_LIMIT:
		movement.x += delta * CAM_SPEED;
		movement.z -= delta * CAM_SPEED;
	
	if mouseMotion.y < MAX_MOUSE_LIMIT:
		movement.z -= delta * CAM_SPEED;
		movement.x -= delta * CAM_SPEED;
		
	elif mouseMotion.y > 1.0 - MAX_MOUSE_LIMIT:
		movement.z += delta * CAM_SPEED;
		movement.x += delta * CAM_SPEED;
	
	self.global_translate(movement);
	
	
func _move_on_click(delta):
	
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		
		var mouseMotion = get_viewport().get_mouse_position();
		var movement = Vector3(0, 0, 0);
		
		mouseMotion /= screenResolution;
		
		mouseMotion.x -= 0.5;
		mouseMotion.y -= 0.5;
		
		movement.x -= mouseMotion.y * delta * CAM_SPEED * 4;
		movement.z -= mouseMotion.x * delta * CAM_SPEED * 4;
		
		self.global_translate(movement);

func _input(event):
	
	if event is InputEventMouseButton and event.pressed:
		
		if event.button_index == BUTTON_RIGHT:
			if room_builder.enabled:
				room_builder._set_enabled(false);
				room_builder._unset_origin();
				set_physics_process(false);
			else:
				event_pos = event.position;
				room_builder._set_enabled(true);
				set_physics_process(true);
				
		if event.button_index == BUTTON_LEFT:
			
			if room_builder.enabled and room_builder.can_build:
				
				if room_builder.debug_enabled:
					room_builder.build_room_debug();
				else:
					room_builder.build_room();
					
				room_builder._set_enabled(false);
				set_physics_process(false);
				
		elif event.button_index == BUTTON_WHEEL_UP:
			
			if room_builder.floors < 3 and room_builder.enabled:
				room_builder.floors += 1;
			
		elif event.button_index == BUTTON_WHEEL_DOWN and room_builder.enabled:
			
			if room_builder.floors > 1:
				room_builder.floors -= 1;

func _physics_process(delta):	

	if room_builder.enabled:
		
		var target = "";
		var result = null;
		
		if !room_builder.has_origin:
			to = self.project_ray_origin(event_pos);
			from = self.project_ray_normal(event_pos) * RAY_LENGTH;
			var space_state = get_world().get_direct_space_state();
			result = space_state.intersect_ray(from, to, [self]);
			target = "origin";
			
		else:
			
			to = self.project_ray_origin(get_viewport().get_mouse_position());
			from = self.project_ray_normal(event_pos) * RAY_LENGTH;
			var space_state = get_world().get_direct_space_state();
			result = space_state.intersect_ray(from, to, [self]);
			
		_build_room(result.get("position"), target);
		
func _get_world_mouse_point(position):

	from = self.project_ray_origin(position)
	to = from + self.project_ray_normal(position) * RAY_LENGTH;

func _build_room(position, target):
	
	if position != null:
		
		var px = floor(position.x);
		var py = floor(position.y);		
		var pz = floor(position.z);
		
		if int(px) % 2 != 0:
			px += 1;
			
		if int(pz) % 2 != 0:
			pz += 1;

		if target == "origin":
		
			room_builder._set_origin(Vector3(px, py, pz));
			
		else:
			
			room_builder.final_pos = Vector3(px, py, pz);
			
